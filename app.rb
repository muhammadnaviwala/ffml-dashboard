require 'sinatra'
require 'firebase'

# GET methods start

get '/' do
	json = "{
		labels: [\"Astros\", \"Rockets\", \"Texans\", \"Dynamo\"],
		datasets: [{
			label: 'Houston sports teams',
			data: [10, 7, 9, 4],
			backgroundColor: [
				'rgb(4, 30, 66)',
				'rgb(186, 12, 47)',
				'rgb(9, 31, 44)',
				'rgb(236, 145, 18)'
			]
		}]
	}"
	erb :index, :locals => { :jsonData => json }
end

get '/table' do
	response = getResponseFromFirebaseNode("Sports")
	erb :table, :locals => { :jsonData => JSON.parse(response.raw_body) }
end

# GET methods end



# Helper methods start

def getResponseFromFirebaseNode (nodeName)
	base_uri = 'https://parffml-69c30.firebaseio.com/'
	firebase = Firebase::Client.new(base_uri)
	firebase.get(nodeName)
end

# Helper methods end
