# README #

README for FFML dashboard. Slack channel: https://pariveda.slack.com/messages/C6AM987SP

### About ###

* Quick summary goes here

### Environment setup ###

* Ensure you have Ruby installed. You can check by running `ruby -v` in terminal/command line. If you don't have Ruby installed, this tutorial should be helpful: https://www.ruby-lang.org/en/documentation/installation/
* Ensure you have the bundler gem installed. Run `sudo gem install bundler` if you don't

### Running the app ###

* In the root directory, execute `bundle install` (required only once, unless new gems get added to `Gemfile`)
* Execute `rackup` to start the server
* Navigate to `localhost:xxxx` where `xxxx` is a port number assigned to you when running the rack command. E.g.: `localhost: 9292`
* If you modify any views (e.g.: `index.erb` or `layout.erb`) you don't have to restart the server. If you modify anything in `app.rb`, you'll have to restart the server by re-running `rackup`

### Documentation references ###

* Sinatra: http://www.sinatrarb.com/intro.html
* Chart.js: http://www.chartjs.org/samples/latest/

### Who do I talk to? ###

* Muhammad Naviwala
* Brian Salato
